<?php
namespace app\push\controller;
use Workerman\Worker;
use Workerman\Lib\Timer;


class Index
{
    private $s = 66666;
    private $type = 1;//1💪 强壮，2，下注，3，开住，4发放
    private $ai;
    private $diannum = 0;
    private $jqdamoney = 0;
    private $jqxiaomoney = 0;
    
    // 
    private $qiangzhuangtime = 30; // 抢庄时间
    private $xiazhutime = 60; // 下注时间
    private $jisuanjieguotime = 5; // 计算结果时间
    private $jiesuantime = 10; // 结算时间
    private $oneOB = 15; // 第一个机器人抢庄时间
    private $twoOB = 3; // 第二个机器人抢庄时间
    private $OneObMoneyMin = 200000; // 第一个机器人抢庄最小金额
    private $OneObMoneyMax = 250000; // 第一个机器人抢庄最大金额
    private $TwoObMoneyMin = 251000; // 第二个机器人抢庄最小金额
    private $TwoObMoneyMax = 300000; // 第二个机器人抢庄最大金额
    
    
    public function index()
    {
        // 创建一个Worker监听2346端口，使用websocket协议通讯
        $ws_worker = new Worker("tcp://0.0.0.0:2346");
        // 启动1个进程对外提供服务
        $ws_worker->count = 1;
        
        $ws_worker->onConnect = function ($connection){
            $bbh = ckeckBb($connection);
            send($bbh, $connection);
            echo "有人链接游戏\n";
        };
        
        //设置心跳保持连接
        $ws_worker->onWorkerStart = function($worker) {
            
            
            
            /* 
             * 设置redis房间
             * 
             * */
            /* redisd()->del("homes");
            $sdsd=redisd()->HSet("homes", 1, array("role_user_id"=>0,
                "rolemoney"=>0,
                "nickname"=>0,
                "createid"=>0
            ));
            dump($sdsd);
            $dsd=redisd()->HgetAll("homes");
            dump($dsd); */
            
            
            
            Timer::add(5, function()use($worker){
                $time_now = time();
                foreach($worker->connections as $connection) {
                    // 有可能该connection还没收到过消息，则lastMessageTime设置为当前时间
                    if (empty($connection->lastMessageTime)) {
                        $connection->lastMessageTime = $time_now;
                        continue;
                    }
                    // 上次通讯时间间隔大于心跳间隔，则认为客户端已经下线，关闭连接
                    if ($time_now - $connection->lastMessageTime > 600) {
                        $connection->close();
                    } 
                }
            });
               
                Timer::add(1, function()use($worker){
                    $chi = redisd()->exists("chifenOB");
                    if ($chi){
                        $chiState = redisd()->get("chifenOB");
                    }else {
                        $chiState = db('config') -> find('11');
                        redisd()->set("chifenOB", $chi);
                    }
                    if ($chiState > 0){
                        $s = date("His");
                        if($s == "201111"||$s == "211111"||$s == "221111"||$s == "231111"||$s == "001111"||$s == "011111"||$s == "021111"){
                            $conModel = db("config");
    //                         机器人自动吃分
                            $conf = $conModel->where("id=5")->find();
                            $co  = $conf['values']-1000;
                            $array = ["values"=>$co];
                            $conModel->where("id=5")->update($array);
    //                         吃分统计
                            $bugConf = $conModel->where("id=10")->find();
                            $cos  = $bugConf['values']+1000;
                            $arrays = ["values"=>$cos];
                            $conModel->where("id=10")->update($arrays);
                        }
                    }
                    //时间计时
                    $this->jishi($worker);
                    
                });
        };
        // 当收到客户端发来的数据后返回hello $data给客户端
        $ws_worker->onMessage = function($connection, $messages)
        {     
            $action = actionAll();//注册方法
            $array = explode("^", $messages);
            foreach ($array as $data){
                if($data){
                    $json = verificationData($data);//验证数据格式
                    if($json){
                        $message = json_decode($data,true);
                        if ($message['action'] != 0) echo "---------客户端发送数据---------".$data."\n";
                        if($message['action'] < 2020 && $action[$message['action']]){//公共方法操作
                            $return = $action[$message['action']]($message,$connection);//处理数据
                            if(($message['action'] == "2010" || $message['action'] == "2015") && $return['ok']=='1'){//如果是客户登陆，修改链接的id
                                //1初始化通信密钥
                                foreach ($connection->worker->connections as $con){
                                    if(isset($con->uid)){
                                        if($con->uid==$return['id']){
                                            $con->close();
                                        }
                                    }
                                }
                                $connection->uid = $return['id'];
                                $users=redisd()->HKeys('user');
                                if(!$users || !in_array($return['id'], $users)){
                                    $userdata=db('users')->where('id='.$return['id'])->find();
                                    redisd()->HSet('user',$return['id'],$userdata);
                                    $res = redisd()->HGet("user", $return['id']);
                                }
                                $kefu = db('gametext')->find(2);
                                send(array("ws"=>38,"content"=>$kefu['text']), $connection);
                                echo $connection->uid."-------登录成功\n";
                            }
                        }else if($action[$message['action']]){
                            $message['s'] = $this->s;
                            $message['t'] = $this->type;
                            $return = $action[$message['action']]($message,$connection);//处理数据
                            if($message['action']==2024||$message['action']==2022){
                                if (isset($connection->homeid)){                                    
                                    echo $connection->uid."加入房间".$connection->homeid."\n";
                                }
                            }
                        }
                        $connection->lastMessageTime = time();
                    }else{
                        $return = array("ok"=>2,"content"=>"加密验证不通过");
                    }
                    send($return, $connection);
                }
            }
        }; 
      $ws_worker->onClose = function($connection){//又玩家推出游戏
            if(isset($connection->uid)){
                echo $connection->uid." login out \n";
            }else{
                echo $connection->id." login out \n";
            }
            if (isset($connection->homeid)){
                $homeUsers = redisd() -> HGet("homeUser", $connection->homeid);
                if ($homeUsers){
                    if (in_array($connection->uid, $homeUsers)){
                        $newHomeUsers = array_diff($homeUsers, [$connection->uid]);
                        redisd() -> HSet("homeUser", $connection->homeid, $newHomeUsers);
                        $josn = array(
                            "ws"=>84,
                            "ren"=>rand(80,100)
                        );
                        getHomeConnections($connection,$josn);
//                         $connection->homeid = null;
                    }
                }
            }
        };
        // 运行worker
        Worker::runAll();

    }
  
    
    /**
     * 计时器，按照计时器计算
     * @param unknown $worker
     */
    private function jishi($worker){

        $SzConTime = redisd()->HgetAll("SzConTime");
        if ($SzConTime){
            foreach ($SzConTime as $k => $v){
                if ($k == "qztime") $this->qiangzhuangtime = $v;
                if ($k == "xztime") $this->xiazhutime = $v;
                if ($k == "jsjgtime") $this->jisuanjieguotime = $v;
                if ($k == "jstime") $this->jiesuantime = $v;
            }
        }else {
            $SzConTime = db('config') -> where('id','in',[14,15,16,17]) -> select();
            foreach ($SzConTime as $v){
                redisd()->HSet("SzConTime", $v['conkey'], $v['values']);
                if ($v['conkey'] == "qztime") {
                    $this->qiangzhuangtime = $v['values'];
                }else if ($v['conkey'] == "xztime") {
                    $this->xiazhutime = $v['values'];
                }else if ($v['conkey'] == "jsjgtime") {
                    $this->jisuanjieguotime = $v['values'];
                }else if ($v['conkey'] == "jstime") {
                    $this->jiesuantime = $v['values'];
                }
            }
        }
        if ($this->s == 66666) {
            $this->s = $this->qiangzhuangtime;
            $m = $this->qiangzhuangtime;
        }else {
            $m = $this->s;
        }
        $this->s = $m - 1;
        $a = $this->type;
        if($a == 1){
            if ($this->qiangzhuangtime > $this->oneOB){
                $ObConf = 16;
            }else {
                $ObConf = 4;
            }
            if ($m == $ObConf){
                $conModel = db("config");
                $co = $conModel->where("id=9")->find();
                $this->ai = $co['values'];
            }
        }
        if($m<=0){
           $data = array();
            $a = $this->type;
            if($a==1){
                echo "开始强壮\n";
                //抢庄结束修改逻辑发送
                $this->s = $this->xiazhutime; // 下注时间
                $this->type = 2;   
                //发送开始下注
            }else if($a==2){
                //下注结束修改逻辑发送 
                $this->s = $this->jisuanjieguotime;
                $this->type = 3;
                //发送开始转骰子
                
                //2大厅计算
                $this->createRootDain();
                echo "大厅计算完毕\n";
                //计算每个人的输赢
                $this->setOrderStatus();
            }else if($a==3){
                echo "开始开奖\n";
                //开住结束修改逻辑发送
                $this->s = $this->jiesuantime;
                $this->type = 4;
                //计算结束 发放点数，发钱
                $this->sendMoneyAll($worker);
            }else if($a==4){
                redisd()->del("unityszdian");
                //结算    
                $this->s = $this->qiangzhuangtime; // 抢庄时间
                $this->type = 1;
                //更新期数计时器
                $conModel = db("config");
                $co = $conModel->where("id=2")->find();
                $num = $co['values'];
                $num = $num+1;
                $conModel->where("id=2")->update(array("values"=>$num));
               
                
                $connecitons = $worker->connections;
                //判断用户是否掉线
                $arrayuser=array();
                foreach($connecitons as $con){
                    if(isset($con->uid)){
                        array_push($arrayuser, $con->uid);
                    }
                } 
                $homeUser=redisd()->HgetAll('homeUser');//二维数组
                if ($homeUser){
                    foreach ($homeUser as $ks=>$w){
                        foreach($w as $key=>$s){
                            if(!in_array($s,$arrayuser)){
                                echo $s."不在线删\n";
                                unset($w[$key]);
                            }
                        }
                        redisd()->HSet('homeUser', $ks, $w);
                    }
                }
                //删除掉线的人
                $User=redisd()->HgetAll('user');//获取所有的用户
                if ($User){
                    foreach ($User as $ksu=>$wu){
                        if(!in_array($ksu,$arrayuser)){
                                echo $ksu."用户不在线删\n";
                                redisd()->HDel('user', $ksu);
                         }
                        
                    }
                }
                //删除房间订单表和房间信息
                $homeidArray=redisd()->HgetAll('homes');//房间的id key
                if($homeidArray){
                    foreach($homeidArray as $k=>$hd){
                        redisd()->del('order'.$k.'7');//当前房间小点的用户id
                        redisd()->del('order'.$k.'8');//当前房间小点的用户id
                        $homeUsers=redisd()->HGet('homeUser',$k);//获取房间内的人
                        echo count($homeUsers);
                        if($k!=1&&count($homeUsers)==0){
                            echo "成功删除房间{$k}\n";
                            redisd()->HDel('homes',$k);
                            redisd()->HDel('homeUser',$k);
                        }
                    }
                }
                
                // 清空庄家
                $array = array("ws"=>86,"ok"=>1,"money"=>0);
                $Uhomes = redisd() -> HGet("homes", 1);
                $Uhomes['role_user_id'] = 0;
                $Uhomes['rolemoney'] = 0;
                redisd() -> HSet("homes",1, $Uhomes);
                sendByHomeIdAndWorker($worker,1,$array);
            }
            //更新所有房间人物的时间阶段信息
            $data['ws'] = 60;
            $data['miao'] = $this->s;
            $data['t'] = $this->type;
            sendAllConnections($worker, $data);
        }
        if($this->ai == 1){
            $userModel = db("users");
            $users = $userModel->where("user_type=1")->select();
            $b=rand(0,count($users)-1);
            $user = $users[$b];
            $user['user_money'] = rand($this->OneObMoneyMin,$this->OneObMoneyMax);
            $home = redisd() -> HGet("homes",1);
            //机器人抢庄
            if($this->type == 1 && $this->s == $this->oneOB){  //第一次抢庄
                if($home['role_user_id'] == 0){
                    echo "机器人抢庄11111111\n";
                    jiqizhuang($worker,$user);
                }
            }elseif ($this->type == 1 && $this->s == $this->twoOB) {
                if($home['role_user_id'] == 0){
                    $user['user_money'] = rand($this->OneObMoneyMin,$this->TwoObMoneyMax);
                    echo "机器人抢庄222222222\n";
                    jiqizhuang($worker,$user);
                }else {
                    $isinjqZhuang=$userModel->where("id={$home['role_user_id']}")->find();
                    if($isinjqZhuang && $isinjqZhuang['user_type'] == 1){//机器人坐庄  换庄家
                        $s1=null;
                        while (true){
                            $s1=rand(0,count($users)-1);
                            if($b != $s1){
                              break;  
                            }
                        }
                        $userR=$users[$s1];
                        $userR['user_money'] = rand($this->TwoObMoneyMin,$this->TwoObMoneyMax);
                        echo "机器人抢庄3333333333333\n";
                        jiqizhuang($worker,$userR);
                    }
                }
            }
            //大厅房间机器人自动下注
            $home= redisd()->HGet('homes',1);
            $u = $userModel->where("id={$home['role_user_id']}")->find();
            if($this->type == 2 && $home['role_user_id'] > 0){
                $s = $this->s;
                $y = (int)$s;
                if( ($y)%2 == 0 && $y > 9){
                    echo "机器人下注\n";
                    //下注
                    aiBet($worker,$u);
                }
            } 
        }
    }
    
 
    /**
     * 计算大厅的点数
     */
    private function createRootDain(){
        $dianModel = db("dian");
   
        //获取庄家id
        $redis=redisd();
        $home=$redis->HGet('homes',1);
        $userid=$home['role_user_id'];
        $num = getNum();
        
        $user = db("users")->where("id={$userid}")->find();
        
        $conModel = db("config");
        $zuida = $conModel->where("id=4")->find();
        $zui = $zuida['values'];//最大数值
        $jishu = $conModel->where("id=5")->find();
        $jis = $jishu['values'];//基数
        
            $daKey= redisd() -> HKeys("order18");//获取大小key
            $xiaoKey= redisd() -> HKeys("order17");
           //ren 
            $ymoney=0;
            $smoney=0;
            //机器人
            $ymoneyJi=0;
            $smoneyJi=0;
            
            
            if($daKey){
                foreach($daKey as $v){
                    $arr=explode('_', $v);
                    if($arr[0]!='jq'){
                        $dalist=redisd() -> HGet("order18",$v);
                        if ($dalist){
                            foreach($dalist as $s){
                                $ymoney+=$s;
                            }
                        }
                    }else{//机器人下注
                        $dalistJi=redisd() -> HGet("order18",$v);
                        if ($dalistJi){
                            foreach($dalistJi as $sj){
                                $ymoneyJi+=$sj;
                            }
                        }
                    }
                    
                } 
            }
            if($xiaoKey){
                foreach($xiaoKey as $vl){
                    $arr=explode('_', $vl);
                    if($arr[0]!='jq'){
                        $xiaolist=redisd() -> HGet("order17",$vl);
                        if ($xiaolist){
                            foreach($xiaolist as $sl){
                                $smoney+=$sl;
                            }
                        }
                    }else{//机器人下注
                        $xiaolistJi=redisd() -> HGet("order17",$vl);
                        if ($xiaolistJi){
                            foreach($xiaolistJi as $slj){
                                $smoneyJi+=$slj;
                            }
                        }
                    }
            
                }
            }
            $status = 1;
            $money = 0;
            $a = 1;
            //查询缓存中有没有人下注  以及机器人下了多少住
            if($ymoney==0 && $smoney==0 && $user['user_type']!=1){//机器人下注    玩家没有下注 并且玩家坐庄
                
                $this -> jqdamoney = $ymoneyJi;
                $this -> jqxiaomoney = $smoneyJi;
                if($ymoneyJi > $smoneyJi){
                    $status = 2;
                    echo "ds\n";
                    $money = $ymoneyJi-$smoneyJi;
                }else{
                    $status = 3;
                    echo "ds1\n";
                    $money = $smoneyJi-$ymoneyJi;
                }
                echo $ymoneyJi."\n";
                echo $smoneyJi."\n";
                //平台可以赢钱
                if(($money+$jis)<$zui){//500-5000<1000
                    $a=2;
                }
                //平台可以输钱
                $fzui = $zui-$zui-$zui;//1000-1000-1000=-1000
                if(($jis-$money)>$fzui){//2000-500
                    $a=3;
                }
                
                if($a==1){
                    $a=2;
                }
                $daxiao = 0;//2大，1小
                //赢钱
                if($a==2){//可以赢
                    if($status==2){//大 500  大  
                        $daxiao = 2;
                        echo $daxiao."a1\n";
                    }else{//小 大
                        $daxiao = 1;
                        echo $daxiao."a2\n";
                    }
                    echo $daxiao."a\n";
                    $conModel->where("id=5")->update(array("values"=>($jis+$money)));
                }else if($a==3){
                    //输钱
                    if($status==2){//大大
                        $daxiao = 2;
                        echo $daxiao."a3\n";
                        
                    }else{
                        $daxiao = 1;
                        echo $daxiao."a4\n";
                    }
                    echo $daxiao."b\n";
                    $conModel->where("id=5")->update(array("values"=>($jis-$money)));
                }
            }else{//正常
                
                if($ymoney>$smoney){
                    $status = 2;
                    $money = $ymoney-$smoney;
                }else{
                    $status = 3;
                    $money = $smoney-$ymoney;
                }
                
                //可以赢钱
                if(($money+$jis)<$zui){
                    $a=2;
                }
                //可以输钱
                $fzui = $zui-$zui-$zui;
                if(($jis-$money)>$fzui){
                    $a=3;
                }
                
                if($a==1){
                    $a=2;
                }
                $daxiao = 0;//2大，1小
                //赢钱
                if($a==2){
                    if($status==2){
                        $daxiao = 1;
                    }else{
                        $daxiao = 2;
                    }
                   
                    $conModel->where("id=5")->update(array("values"=>($jis+$money)));
                }else if($a==3){
                    //输钱
                    if($status==2){
                        $daxiao = 2;
                    }else{
                        $daxiao = 1;
                    }
                    $conModel->where("id=5")->update(array("values"=>($jis-$money)));
                }
            }
            
            //计算所有点
            $ay = getdian();
            $dian = "";
           
            if($daxiao==1){
                   $dian=$ay[rand(0,104)];
            }else{
                   $dian=$ay[rand(105,209)];
            }
            echo $dian."\n";
            $this -> diannum = $dian;
            $array = array(
                "home_id"=>1,
                "num"=>$num,
                "dian"=>$dian,
                "add_time"=>time()
            );
     
            if($ymoney==0 && $smoney==0 && $user['user_type'] == 1){
                $dian = getdian();
                $array = array(
                    "home_id"=>1,
                    "num"=>$num,
                    "dian"=>$dian[rand(0,209)],
                    "add_time"=>time()
                );
              
            }
        $dianModel->insert($array);
    }
    
    /**
     * 计算下注的人的记录
     */
    private function setOrderStatus(){
        $num = getNum();
        $orderModel = db("order");
        $dianModel = db("dian");
        $userModel = db("users");
        //本期所有非机器人下注的订单
        
        //计算除了机器人下注的所有订单   插入到数据库当中
        
        //1.homes  key
        $homesKey=redisd()->HKeys('homes');
        //2.用key去查当前房间里面的 下注信息
        $orderxiao=0;
        $orderda=0;
        $orderData=array(
            'add_time'=>time(),
            'num'=>$num,
            'is_winning'=>0,
            'order_type'=>0,
        );
        if($homesKey && count($homesKey)>0){
            foreach($homesKey as $v){
               $xiaoUserId= redisd()->HKeys('order'.$v.'7');//当前房间小点的用户id
               //xiao
               if($xiaoUserId){//下注了
                   foreach($xiaoUserId as $s){
                       $useridArray=explode('_', $s);
                       if($useridArray[0]!='jq'){
                           $order=redisd()->HGet('order'.$v.'7', $s);//获取到当前房间下当前用户的 大小点下注记录
                           $orderxiao=array_sum($order);//当前用户下小点订单总和
                           if($orderxiao&&$s>0){
                               $orderData['user_id']=$s;
                               $orderData['home_id']=$v;
                               $orderData['money']=$orderxiao;
                               $orderData['dian']=7;
                               $orderModel->insert($orderData);
                           }
                       }
                   } 
               }
               $daUserId= redisd()->HKeys('order'.$v.'8');//当前房间大点的用户id
               //da
               if($daUserId){//下注了
                   foreach($daUserId as $ds){
                       $order=redisd()->HGet('order'.$v.'8', $ds);//获取到当前房间下当前用户的 大小点下注记录
                       $orderda=array_sum($order);//当前用户下小点订单总和
                       if($orderda && $ds>0){
                           $orderData['user_id']=$ds;
                           $orderData['home_id']=$v;
                           $orderData['money']=$orderda;
                           $orderData['dian']=8;
                           $orderModel->insert($orderData);
                       }
                   }
               }
            }
        }
        
        $orderList = $orderModel->where("is_winning=0 and num={$num} and order_type=0")->select();
        //本期所有的开住记录
        $dianList = $dianModel->where("num={$num}")->select();
        $ids = "";
        foreach ($orderList as $order){
            $orderid = $order['id'];
            $homeid= $order['home_id'];
            foreach ($dianList as $dian){
                if($homeid==$dian['home_id']){
                    $d = $dian['dian'];
                    $ds = explode("-", $d);
                    
                    $diannum = $ds[0]+$ds[1]+$ds[2];
                 
                    if($ds[0]==$ds[1]&&$ds[1]==$ds[2]){
                        
                    }else{
                        if($order['dian']==7){
                            if($diannum<=10){
                                $ids = $ids.",".$orderid;
                            }
                        }else if($order['dian']==8){
                            if($diannum>=11){
                                $ids = $ids.",".$orderid;
                            }
                        }
                    }
                   break;
                }
            }
        }
        $ids = trim($ids,",");
        //修改所有赢的订单
        if($ids){
            $orderModel->where("id in(".$ids.")")->update(array("is_winning"=>1));
        }
        
        //获取赢的订单的人，并发放
        $yong = db("config")->where("id=3")->find();
        $oList = $orderModel->field("sum(money) as money,user_id,is_winning,home_id")->where("is_winning=1 and num={$num} and order_type=0")->group("user_id")->select();
        foreach ($oList as $o){
            $countmoney = 0;
            $countmoney = $o['money']+($o['money']-($o['money']*$yong['values']));
            $countmoney = floor($countmoney);
            $userModel->where("id={$o['user_id']}")->setInc("user_money",$countmoney);
            $redisUser = redisd() -> HGet("user", $o['user_id']);
            $redisUser['user_money'] = $redisUser['user_money'] + $countmoney;
            redisd() -> HSet("user", $o['user_id'], $redisUser);
            
            if($o['home_id']!=1&&$o['is_winning']==1){//其他房间的额
                $homes=redisd()->HGet('homes',$o['home_id']);//获取当前房间里面de信息
                $createid=redisd()->HGet('user',$homes['createid']);//
                if( $homes['createid']&&isset($createid)){//存在房主
                    //添加代理的钱
                    $createid['user_money']+=$o['money']*0.03;
                    $createid['com_money']+=$o['money']*0.03;
                    $createid=redisd()->HSet('user',$homes['createid'],$createid );
                    $userModel->where("id={$homes['createid']}")->update(array("user_money"=>$createid['user_money'],"com_money"=>$createid['com_money']));
                    $dlmoney = array(
                        "user_id" => $homes['createid'],
                        "money"   => $o['money']*0.03,
                        "buy_id"  => $o['user_id'],
                        "add_time"=> time()
                    );
                    db("rebate") -> insert($dlmoney);
                }
            }
        }
        //计算庄家的信息和输赢
        $homeList = redisd() -> HgetAll("homes");
        if ($homeList){
            foreach ($homeList as $k => $home){
                $roleid = $home['role_user_id'];//庄家id
                if($roleid){
                    $user = redisd() -> HGet("user", $roleid);
                    if($user){//机器人将不计算，人计算
                        $zmoney = $user['user_money'];//庄家钱
                        $ymoney = $orderModel->where("is_winning=1 and num={$num} and home_id={$k} and order_type=0")->sum("money");
                        $smoney = $orderModel->where("is_winning=0 and num={$num} and home_id={$k} and order_type=0")->sum("money");
                        $mon = ($zmoney+$smoney)-$ymoney;
                        //庄家赢钱扣分
                        $hm = 0;
                        
                        if($smoney-$ymoney>0){ // 庄家赢钱
                            $kmoney = ($smoney-$ymoney)*$yong['values'];//庄家赢的钱
                            $mon = $zmoney+$kmoney;
                            $hm = $kmoney;
                            $type = 1;
                        }elseif ($smoney == $ymoney && $smoney == 0){
                        /* 
                            $dajq = $this->jqdamoney;
                            $xiaojq = $this->jqxiaomoney;
                            $dianshu = $this->diannum;
                            $ds1 = explode("-", $dianshu);
                            $diadsds = $ds1[0]+$ds1[1]+$ds1[2];
                            echo "机器人压小小小小小小总数".$xiaojq."\n";
                            echo "机器人压大大大大大大总数".$dajq."\n";
                            echo "开点".$dianshu."\n";
                            echo "点数".$diadsds."\n";
                            if ($diadsds > 10){ //大于10 开大
                                echo "进入开大\n";
                                if ($xiaojq > $dajq){ 
                                    echo "小 》》 大 \n";
                                    $kmoney = $xiaojq - $dajq - ($xiaojq - $dajq) * $yong['values'];//庄家赢的钱
                                    $mon = $zmoney + $kmoney;
                                    $hm = $kmoney;
                                    $type = 1;
                                }elseif ($xiaojq == $dajq){
                                    echo "相等\n";
                                    $mon = $zmoney;
                                }elseif ($xiaojq < $dajq) {//zhuang shu
                                    echo "大 》》小\n";
                                    $mon = $zmoney - ($dajq -$xiaojq);
                                    $hm = $dajq - $xiaojq;
                                    $type = 0;
                                }
                            }else {//开小
                                echo "进入开小\n";
                                if ($xiaojq < $dajq){//大>小
                                    echo "大》》小\n";
                                    $kmoney = $dajq - $xiaojq - ($dajq - $xiaojq) * $yong['values'];//庄家赢的钱
                                    $mon = $zmoney + $kmoney;
                                    $hm = $kmoney;
                                    $type = 1;
                                }elseif ($xiaojq == $dajq){
                                    echo "相等\n";
                                    $mon = $zmoney;
                                }elseif ($xiaojq > $dajq) {
                                    echo "小》》大\n";
                                    $mon = $zmoney - ($xiaojq -$dajq);
                                    $hm = $dajq - $xiaojq;
                                    $type = 0;
                                }
                            }
                         */
                        
                        }else {
                            $hm = $ymoney-$smoney;
                            $type = 0;
                        }
                        
                        if ($hm != 0){
                            $insert = array(
                                "home_id" => $k,
                                "user_id" => $roleid,
                                "add_time"=> time(),
                                "num"     => $num,
                                "money"   => $hm,
                                "dian"    => 1,
                                "is_winning"=> $type,
                                "order_type"=> 0
                            );
                            $orderModel->insert($insert);
                        }
                        $userModel->where("id={$roleid}")->update(array("user_money"=>$mon));
                        $redisUser = redisd() -> HGet("user", $roleid);
                        $redisUser['user_money'] = $mon;
                        redisd() -> HSet("user", $roleid, $redisUser);
                    }
                }
            }
        }
        //计算输家
        $homeUsers = redisd() -> HgetAll("homeUser");
        if ($homeUsers){
            foreach ($homeUsers as $k => $v){
                foreach ($v as $row){
                    $res = explode("_", $row);
                    if ($res[0] != "jq"){
                        $myUser = redisd() -> HGet("user", $row);
                        db("users") -> where("id={$row}") -> update(array("user_money"=>$myUser['user_money']));
                    }
                }
            }
        }
    }
    /**
     * 发放每个人的积分205` *n $worker
     */
    private function sendMoneyAll($worker){
        $orderModel = db("order");
        $num = getNum();
        $yong = db("config")->where("id=3")->find();
        $dianModel = db("dian");
        foreach ($worker->connections as $connection){
            //获得在先的每个人
            if(isset($connection->uid)&&isset($connection->homeid)){
                $dian = $dianModel->where("num={$num} and home_id={$connection->homeid}")->find();
                //获取缓存当中的  房间
                $home=redisd()->HGet('homes',$connection->homeid);
                if($home['role_user_id']==$connection->uid){
                    echo "庄家结算\n";
                    $ymoney = $orderModel->where("is_winning=1 and num={$num} and home_id={$connection->homeid} and order_type=0 and user_id!={$connection->uid}")->sum("money");
                    $smoney = $orderModel->where("is_winning=0 and num={$num} and home_id={$connection->homeid} and order_type=0 and user_id!={$connection->uid}")->sum("money"); 
                    $xianmoney = getmoney($connection->uid); //获取缓存剩余的钱
                    //$zmoney = $home['rolemoney'];
                    if($smoney>$ymoney){
                        $ymoney = $smoney-$ymoney;
                        $ymoney = $ymoney-($ymoney*$yong['values']);
                        $a = 2;
                        echo "赢 \n";
                    }else if($smoney<$ymoney){
                        $ymoney = $ymoney-$smoney;
                        $a = 3;
                        echo "输 \n";
                    }else if($smoney==$ymoney){
                        $a = 4;
                        $ymoney =0;
                        echo "不输不赢 \n";
                        /* 
                        if ($smoney == 0){
                            $daSum   = $this->jqdamoney;
                            $xiaoSum = $this->jqxiaomoney;
                            $dianSum = $this->diannum;
                            $dss1 = explode("-", $dianSum);
                            $diadsdss = $dss1[0]+$dss1[1]+$dss1[2];
                            echo "结算==机器人压小小小小小小总数".$xiaoSum."\n";
                            echo "结算==机器人压大大大大大大总数".$daSum."\n";
                            echo "结算==开点".$dianSum."\n";
                            echo "结算==点数".$diadsdss."\n";
                            if ($diadsdss > 10){ //开大
                                echo "结算==进入开大\n";
                                if ($daSum > $xiaoSum){
                                    echo "结算==大 》》 小 \n";
                                    $ymoney = $daSum - $xiaoSum;
                                    $a = 3;
                                }else {
                                    echo "结算==小》》大\n";
                                    $ymoney = $xiaoSum - $daSum;
                                    $ymoney = $ymoney - ($ymoney * $yong['values']);
                                    $a = 2;
                                }
                            }else { //开小
                                echo "结算==进入开小\n";
                                if ($xiaoSum > $daSum){
                                    echo "结算==小》》大\n";
                                    $ymoney = $xiaoSum - $daSum;
                                    $a = 3;
                                }else {
                                    echo "结算==大 》》 小 \n";
                                    $ymoney = $daSum - $xiaoSum;
                                    $ymoney = $ymoney - ($ymoney * $yong['values']);
                                    $a = 2;
                                }
                            }
                        }else {
                            $a = 4;
                            $ymoney=0;
                        }
                        
                     */
                    }
                    $array = array(
                        "ws"=>82,
                        "iswin"=>$a,
                        "dian"=>$dian['dian'],
                        "money"=>$ymoney,
                        "yue"=>$xianmoney
                    );
                    send($array, $connection);
                }else {
                    echo "玩家结算\n";
                    //赢钱
                    $order = $orderModel->field("sum(money) as money,is_winning")->where("user_id={$connection->uid} and num={$num} and home_id={$connection->homeid} and is_winning=1")->group("user_id")->find();
                    $yymoney = $order['money'];                
                    $yymoney = $yymoney-($yymoney*$yong['values']);
                    //输钱
                    $ssmoney = $orderModel->where("user_id={$connection->uid} and num={$num} and home_id={$connection->homeid} and is_winning=0")->sum("money");
                    $a = 1;//为下注
                    $money = 0;
                    if($yymoney==0&&$ssmoney==0){
                        
                    }else{
                        if($yymoney>$ssmoney){
                            $a = 2;
                            $money = $yymoney-$ssmoney;
                        }elseif($yymoney<$ssmoney){
                            $a = 3;
                            $money = $ssmoney-$yymoney;
                        }else{
                            $a = 4;
                            $money = 0;
                        }
                    }
                    $ye = getmoney($connection->uid);
                    $array = array(
                        "ws"=>82,
                        "iswin"=>$a,
                        "dian"=>$dian['dian'],
                        "money"=>$money,
                        "yue"=>$ye
                    );
                    send($array, $connection);
                }
            }
        }
    }    
}
