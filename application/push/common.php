<?php

// 应用公共文件
use action\User;
use action\Game;
use GameRedis\RedisPackage;
/**
 * 操作方法注册
 * @return string[]
 */
function actionAll(){
    //注册操作方法
    //20一下为公众方法不需要加密
    return array(
        0=>"xintiao",//心跳
        2009=>"refresh",// 刷新金币
        2010=>"login",//登录
        2011=>"getTime",//获取时间
        2012=>"getRankings",// 排行
        2013=>"gameRule",//规则
        2014=>"reg",//注册
        2015=>"watchLogin",
//         16=>"yongjin",
        2017=>"passBack",
        //需要加密业务逻辑
        2020=>"recharge",//充值提交
        2021=>"widthraswals",//提现提交
        2022=>"createHome",//创建房间
        2023=>"inHome",//加入房间
        2024=>"createHomeHeader",//创建头文件
        2025=>"inRootHome",//加入 大厅
        2026=>"zhuang",//转账
        2027=>"rechargeLog",//充值记录
        2028=>"withLog",//提现记录
        2029=>"zhuanLog",//转账记录
        2030=>"robZhuang",//抢庄
        2031=>"bet",//下注
        2032=>"historyRecord",//历史记录
//         33=>"betRecord",//下注记录
        2034=>"notice",//公告
        2035=>"xiazhuang",//下庄
        2036=>"getRen",
        2037=>"outhome",//退出房间
        2038=>"kefu",// 客服
        2110=>"getYj",//佣金
        2111=>"congzi",//充值  二维码 
    );
    
    
    //主动发送
    /* 
     * {"action":2,"data":"{\"content\":\"\\u4f59\\u989d \\u4e0d\\u8db3\"}","sign":"bf51b7c65915a258409089d12d10f474"}
     * 
     * array(
     *  60  主动发送时间
     *  
     *  
        80  抢庄分发
        81  下注分发
        82  分发赢输
        83  喇叭
        84  人数变更
        85  可以下庄
        86  下庄
    ); */
}


function getKey(){
    $key = "5e08323af9116a8824139704e36d7c15";
    return $key;
}

function getRankings(){
    $date = db('users')->field("nickname,user_money")->where("user_type = 0")->order("user_money desc")->limit('0,10')->select();
    if ($date){
        $return = array(
            'ws' => 2012,
            'rankings'=>$date
        );
    }else {
        $return = array(
            'ws' => 2012,
            'rankings'=>0
        );
    }
    return $return;
}


/* *
 * 
 *  充值111
 *  
 *  */

function  congzi($message,$connection){
    $json = json_decode($message['data'],true);
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"uid null");
    }
    
    $gameModel = new Game();
    return $gameModel->congzi($json,$connection);
    
    
}

/**
 * 获取佣金
 */
function getYj($message,$connection){
    $user = redisd() -> HGet("user", $connection->uid);
    if ($user){
        $user['dl'] = "您不是代理，想成为代理赚取佣金请联系客服！";
        return array("ws"=>110,"ok"=>1,"data"=>$user);
    }
}

/**
 * 登录游戏
 * @param unknown $message
 * @return string[]|number[]|\think\db\false|PDOStatement|string|\think\Model
 */
function login($message,$connection){
    $usermodel = new User();
    return $usermodel->login($message,$connection); 
}

//绑定手机
function watch($message){
    $usermodel = new User();
    return $usermodel->wacht($message);
}



/**
 * 获取服务器时间,秒数
 */
function getTime($message){
    $date = array();
    $date['content'] = date("s");
    $date['ok'] = "1";
    return $date;
}

/**
 * 获取游戏规则
 */
function gameRule($message,$connection){
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"用户信息id信息错误，链接为非登录id");
    }
    $gameModel = new Game();
    return $gameModel->gameRule($connection);
}

function kefu($message,$connection){
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"用户信息id信息错误，链接为非登录id");
    }
    $kefu = db('gametext')->find(2);
    return array("ws"=>38,"content"=>$kefu['text']);
}

function refresh($message,$connection){
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"用户信息id信息错误，链接为非登录id");
    }
    $userdata=db('users')->where("id=".$connection->uid)->find();
    redisd()->HSet('user',$connection->uid,$userdata);
    return array("ws"=>9,"money"=>$userdata['user_money']);
}


/**
 * 判断是否是json格式
 * @param 要判断的字符串 $string
 * @return boolean
 */
function is_json($string)
{
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

/**
 * 验证收到信息
 * @param unknown $data
 * @return boolean
 */
function verificationData($data){
    if(is_json($data) && requestData($data)){
        return true;
    }else{
        return false;
    }
}


/**
 * 判断客户发来的信息加密是否正确
 * @param unknown $data
 * @param unknown $con
 * @return boolean
 */
function requestData($data){
    $key = getKey();
    
    $jsondata = json_decode($data,true);
    
    $strmd5 = md5($jsondata['action'].$jsondata['data'].$key);
    
    if($strmd5 == $jsondata['sign']){
        return true;
    }else{
        return false;
    }
}

/**
 * 发送数据
 * @param 协议号 $num
 * @param 数据 $data数组
 * @param 发送给谁 $client_id
 */
function send($return,$connection){
    if (isset($return['ok']) && $return['ok'] == 2){
        $num = 2;
        $return['errorCode'] = 2;
        unset($return['ok']);
    }else {
        if ($return['ws'] == 0){
            $num = 0;
        }else if ($return['ws'] < 2000){
            $num = (int)$return['ws']+2000;
        }else {
            $num = (int)$return['ws'];
        }
        if (isset($return['ok'])) unset($return['ok']);
        unset($return['ws']);
    }
    $data = json_encode($return);
    $key = getKey();
    $sign = md5($num.$data.$key);
    $array = array("action"=>$num,"data"=>$data,"sign"=>$sign);
    $str = json_encode($array);
    sendToByte($connection, $str);
    if($num!=0){
        echo "server:".$str."\n";
    }
    $ss = sendToByte($connection, $str);
    $connection->send($ss);
}

/**
 * 编辑数据成包头包体
 * @param unknown $client_id
 * @param unknown $str
 * @return boolean
 */
function sendToByte($client_id,$str){
    $a = strlen($str);
    if($a>0&&$a<10){
        $c = "000".$a;
    }elseif($a<100){
        $c = "00".$a;
    }elseif($a<1000){
        $c = "0".$a;
    }elseif($a<10000){
        $c = $a;
    }else{
        return false;
    }
    $str = $c.$str;
    return $str;
}

/**
 * 充值提交
 * @param unknown $message
 */
function recharge($message,$connection){
    $json = json_decode($message['data'],true);
   
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"用户信息id信息错误，链接为非登录id");
    }
    if($json['money']<=0){
        return array("ok"=>2,"content"=>"金额错误");
    }
    $gameModel = new Game();
    return $gameModel->recharge($json,$connection->uid);
}
/**
 * 提交提现
 * @param unknown $message
 */
function widthraswals($message,$connection){
    $json = json_decode($message['data'],true);
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"用户信息id信息错误，链接为非登录id");
    }
    if($json['money']<=0){
        return array("ok"=>2,"content"=>"金额错误");
    }
    return array("ok"=>2,"content"=>"请联系群主");
   /*  $gameModel = new Game();
    return $gameModel->widthraswals($json,$connection->uid); */
}

/**
 * 创建房间
 */
function createHome($message,$connection){
    $json = json_decode($message['data'],true);
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"用户信息id信息错误，链接为非登录id");
    }
    $us = redisd() -> HGet("user", $connection->uid);
    if ($us['is_create'] == 0){
        return array("ok"=>2,"content"=>"您没有权限创建房间,请联系管理员添加权限");
    }
    $homeId = 0;
    while (true){
       $homeId = rand(100000,999999);
       $a = redisd() -> HGet("homes",$homeId);
       if(!$a){
          break;
       }
    }
    $connection->homeid = $homeId;
    $cHome = array(
        "home_name"=>$homeId,
        "nickname"=>$us['nickname'],
        "createid"=>$connection->uid,
        "rolemoney"=>"0",
        "role_user_id"=>"0"
    );
    $homeUsers = array($connection->uid);
    redisd() -> HSet("homeUser", $homeId, $homeUsers);
    redisd() -> HSet("homes", $homeId, $cHome);
    return array("ok"=>1,"ws"=>"22","homeid"=>$homeId);
}


/**
 * 加入房间
 * @param unknown $message
 * @param unknown $id
 * @return number[]|string[]
 */
function inHome($message,$connection){
    $json = json_decode($message['data'],true);
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"用户信息id信息错误，链接为非登录id");
    }
    if(!isset($json['homeid'])){
        return array("ok"=>2,"content"=>"房间号错误");
    }
    $homeId = $json['homeid'];
    $gameModel = new Game();
    return $gameModel->inHome($homeId,$connection->uid); 
}

function outhome($message,$connection){
    $json = json_decode($message['data'],true);
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"用户信息id信息错误，链接为非登录id");
    }
    if(!isset($connection->homeid) || !isset($json['homeid']) || $connection->homeid != $json['homeid']){
        return array("ok"=>2,"content"=>"房间号错误");
    }
    if ($connection->homeid){
        $home = redisd() -> HGet("homes",$connection->homeid);
        $oMin = redisd() -> HGet("order".$connection->homeid."7",$connection->uid);
        $oMax = redisd() -> HGet("order".$connection->homeid."8",$connection->uid);
        if ($home['role_user_id'] == $connection->uid || $oMin || $oMax){
            return array("ok"=>2,"content"=>"请耐心等待游戏结束后再离开！");
        }
        $homeUsers = redisd() -> HGet("homeUser", $json['homeid']);
        if ($homeUsers){
            if (in_array($connection->uid, $homeUsers)){
                $newHomeUsers = array_diff($homeUsers, [$connection->uid]);
                redisd() -> HSet("homeUser", $json['homeid'], $newHomeUsers);
                $josn = array(
                    "ws"=>84,
                    "ren"=>rand(80,100)
                );
                getHomeConnections($connection,$josn);
                $connection->homeid = null;
                return array("ws"=>37,"list"=>"退出房间成功");
            }
        }
    }
}

function inRootHome($message,$connection){
    $json = json_decode($message['data'],true);
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"用户信息id信息错误，链接为非登录id");
    }
    if(!isset($json['homeid'])){
        return array("ok"=>2,"content"=>"房间号错误");
    }
    $homeId = $json['homeid'];
    $gameModel = new Game();
    $data = $gameModel->inHome($homeId,$connection->uid);
    $data['ws'] = 25;
    return $data;
}

/**
 * 加入房间后注册房间头
*/
function createHomeHeader($message,$connection){
    $json = json_decode($message['data'],true);
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"用户信息id信息错误，链接为非登录id");
    }
    if(!isset($json['homeid'])){
        return array("ok"=>2,"content"=>"房间号不能为空");
    }
    $gameModel = new Game();
    $data = $gameModel->createHomeHeader($connection,$json['homeid']);
    if($data['ok']==1){
       $data['miao'] = $message['s'];
       $data['t'] = $message['t'];
    }
    return $data;
}
/**
 * 抢庄
 */
function robZhuang($message,$connection){
    $json = json_decode($message['data'],true);
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"uid null");
    }
    if(!isset($connection->homeid)){
        return array("ok"=>2,"content"=>"26fail");
    }
    if($message['t']==1&&$message['s']){
        
    }else{
        return array("ok"=>2,"content"=>"已超时");
    }
    $gameModel = new Game();
    return $gameModel->robZhuang($connection,$json);
}


function getmoney($uid){
    $moneys=redisd()->HGet('user',$uid);
    if(isset($moneys)){
        return $moneys['user_money'];
    }else{
        return 0;
    } 
}




/**
 * 发送数据到同一个房间的人
 * @param 当前链接 $connection
 * @param 发送的数组 $josn
 * 
 */
function getHomeConnections($connection,$josn){
    $worker = $connection->worker;
    $homeid = $connection->homeid;
    $connections = $worker->connections;
    foreach ($connections as $con){
        if(isset($con->homeid)){
                if($con->homeid==$homeid){
                    send($josn, $con);
                }
        }
    }
}

/**
 * 发送数据到所有的在房间的人
 * @param 当前线程  $worker
 * @param 发送的数组 $josn
 * @param 
 */
function sendAllConnections($worker,$josn){
    $connections = $worker->connections;
    foreach ($connections as $con){
        if(isset($con->homeid)){
            send($josn, $con);
        }
    }
   
}

/**
 * 进入房间获得房间的大小主
 * @param unknown $homeid
 * @return number
 */
function getOrderDaXiao($homeid){
    $conModel = db("config");
    $nums = $conModel->where("id=2")->find();
    $num = $nums['values'];
    //获取缓存中的用户 下注记录
    $redis=redisd();
    $res7 =$redis->HVals('order'.$homeid.'7');//获取订单7的下注记录
    $res8 =$redis->HVals('order'.$homeid.'8');//获取订单8的下注记录
   
    $data['xiao']=$res7?daxiaoOrder($res7):0;
    $data['da']=$res8?daxiaoOrder($res8):0;
    
    return $data;
}
/* *
 * 循环获取大小下注
 *  */

function daxiaoOrder($data,$count=0){
    $array8='';
    $num=0;
    foreach($data as $val){
        $array8+=array_sum($val);
        $num+=count($val);
    }
    
    if($count==0){
        return $array8;
    }elseif($count==1){
        return $num;
    }
   
}


/**
 * 在游戏中获取订单数据
 * @param unknown $homeid
 * @return number
 */
function getOrder($homeid){
    $conModel = db("config");
    $nums = $conModel->where("id=2")->find();
    $num = $nums['values'];
    
    $redis=redisd();
    $res7 =$redis->HVals('order'.$homeid.'7');//获取订单7的下注记录
    $res8 =$redis->HVals('order'.$homeid.'8');//获取订单7的下注记录
     
    $data['xiao']=$res7?daxiaoOrder($res7):0;
    $data['da']=$res8?daxiaoOrder($res8):0;
    $num1=$res7?daxiaoOrder($res7,1):0;
    $num2=$res8?daxiaoOrder($res8,1):0;
    $data['num'] = $num1+$num2;
    return $data;
}

/**
 * 下注
 * @param unknown $message
 * @param unknown $connection
 * @return number[]|string[]
 */
function bet($message,$connection){
    $json = json_decode($message['data'],true);
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"uid null");
    }
    if(!isset($connection->homeid)){
        return array("ok"=>2,"content"=>"31 fail");
    }
    
    //查询队列中房间信息
    
    $fff =redisd()->HGet('homes',$connection->homeid);//获取当前房间 里面的信息
    if($fff['rolemoney']==0){
        return array("ok"=>2,"content"=>"无庄家");
    }
    if($fff['role_user_id']==$connection->uid){
        return array("ok"=>2,"content"=>"庄家无法押注");
    }
    if($message['t']!=2){
        return array("ok"=>2,"content"=>"不到下注时间");
    }
    $gameModel = new Game();
    $gameInfo = $gameModel->bet($json,$connection); 
    return $gameInfo;
}

/**
 * 计算点
 */
function getdian(){
    $array = array();
    for($i = 1;$i<=6;$i++){
        for($j = 1;$j<=6;$j++){
            for($k = 1;$k<=6;$k++){
                $a = $i+$j+$k;
                
                if($i==$j&&$j==$k){
                    continue;
                }
                $b = $i."-".$j."-".$k;
                if($a>10){
                    array_push($array, $b);
                }else{
                    array_unshift($array, $b);
                }
                
                
            }
        }
    }
    return $array;
}

function jiqizhuang($worker,$user){
    $gameModel = new Game();
    return $gameModel->jiqirob($worker,$user);
}
function aiBet($worker,$user){
    $gameModel = new Game();
    return $gameModel->aibet($worker,$user);
}

/**
 * ai发送订单信息
 * @param unknown $worker
 * @param unknown $josn
 */
function AiSendOrder($worker,$josn){
    $connections = $worker->connections;
    foreach ($connections as $con){
        if(isset($con->homeid)){
            if($con->homeid==1){
                send($josn, $con);
            }
        }
    }
}

/**
 * 玩家坐庄机器人下注计数
 */
function ordreMoath(){
    $model= db("ai");
    $js = $model->where("id=1")->find();
    return $js;
}



/**
 * 历史记录
 * @param unknown $message
 * @param unknown $connection
 */
function historyRecord($message,$connection){
    // 测试大厅记录 需更改    
    $connection->homeid = 1;
    
    
    
    $dianModel = db("dian");
    $select = $dianModel->field("dian")->where("home_id={$connection->homeid}")->order("add_time desc")->limit(0,10)->select();
    for($i=0;$i<count($select);$i++){
        $dian = $select[$i]['dian'];
        $dd = explode("-", $dian);
        $a =($dd[0]+$dd[1]+$dd[2]);
        if($a>10){
            $select[$i]['dian'] = $dian."-大";
        }else{
            $select[$i]['dian'] = $dian."-小";
        }  
    }
    return array("ws"=>32,"ok"=>1,"list"=>$select);
}

/**
 * 下注记录
 * @param unknown $message
 * @param unknown $connection
 * @return number[]|\think\Collection[]|\think\db\false[]|PDOStatement[]|string[]
 */
function betRecord(){
    
  
   /*  $array = array();
    $orders[7] = redisd() -> HgetAll("order17");
    $orders[8] = redisd() -> HgetAll("order18");
    foreach ($orders as $k => $v){
        foreach ($v as $kk => $row){
            $ks = explode("_", $kk);
            $users = redisd() -> HGet("user", $kk);
            if (!$users && $ks[0] == "jq"){
                $users = db("users") -> find($ks[1]);
                if ($k == 7){
                    $obnames = array("玩筛子我只压小","红日","小飒","老黑","我要豹子","中原奥特曼","三星","醉听雨","逝然陌","月夜空","夏日枫","神之刺","墨天狼","星烁烁","轩辕绝","宸影殇","武妖帝","寒风澈","大刘");
                    $users['nickname'] = $obnames[array_rand($obnames)];
                }
            }
            foreach ($row as $res){
                $array[] = array(
                    "nickname" => $users['nickname'],
                    "dian"     => $k,
                    "money"    => $res
                );
            }
        }
    } */
//     dump($array);
//     return array("ws"=>33,"ok"=>1,"list"=>$array);
//     $ordreModel = db("order");
    
//     $nums = $ordreModel->field("num")->group("num")->order("num desc")->limit(0,6)->select();
//     foreach ($nums as $num){
       
//         $ymoney = $ordreModel->where("num={$num['num']} and is_winning=1 and user_id={$connection->uid}")->sum("money");
//         if($ymoney>0){
//             $pp = $ordreModel->where("num={$num['num']} and is_winning=1 and user_id={$connection->uid}")->find();
//             $array[] = array(
//                 "money"=>$ymoney,
//                 "is_winning"=>1,
//                 "dian"=>$pp['dian'],
//                 "date"=>date("H:i:s",$pp['add_time'])
//             );
//         }
//         $smoney = $ordreModel->where("num={$num['num']} and is_winning=0 and user_id={$connection->uid}")->sum("money");
//         if($smoney>0){
//             $pc = $ordreModel->where("num={$num['num']} and is_winning=0 and user_id={$connection->uid}")->find();
//             $array[] = array(
//                 "money"=>$smoney,
//                 "is_winning"=>0,
//                 "dian"=>$pc['dian'],
//                 "date"=>date("H:i:s",$pc['add_time'])
//             );
//         }
//     }

}
/**
 * 发送公告
 * @param unknown $message
 * @param unknown $connection
 */
function notice($message,$connection){

     $json = json_decode($message['data'],true);
     if(!isset($connection->uid)){
         return array("ok"=>2,"content"=>"uid null");
     }
     
     $gameModel = new Game();
     return $gameModel->notice($json,$connection);
     
     
}



function reg($message){
    
    $usermodel = new User();
    return $usermodel->reg($message);
}


function watchLogin($message,$connection){
    $user = new User();
    return $user->watchLogin($message,$connection);
}

/**
 * 转账
 * @param unknown $message
 * @param unknown $connection
 * @return number[]|string[]
 */
function zhuang($message,$connection){
    
    $json = json_decode($message['data'],true);
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"uid null");
    }
    $userModel = db("users");
    $toid = $json['id'];
    $money =  floor($json['money']);
    $user = $userModel->find($toid);
    if($user){//判断目标账户是否有
        //庄家不能做转账
        $homeAll = redisd() -> HVals("homes");
        foreach ($homeAll as $homes){
            if ($homes['role_user_id'] == $user['id'] || $homes['role_user_id'] == $connection->uid){
                return array("ok"=>2,"content"=>"玩家坐庄时无法进行转账");
            }
        }
        //查找自己用户信息
        $myuser = $userModel->where("id={$connection->uid}")->find();
        if($myuser['user_money'] < $money || $money <= 0){
            return array("ok"=>2,"content"=>"余额不足");
        }
        if($connection->uid == $user['id']){
            return array("ok"=>2,"content"=>"不能自己给自己转账！");
        }
        //修改用户信息，转账
        $userModel->where("id={$connection->uid}")->setDec("user_money",$money);
        $userModel->where("id={$user['id']}")->setInc("user_money",$money - ($money*0.05));
        $userDec = redisd() -> HGet("user", $connection->uid);
        $userInc = redisd() -> HGet("user", $user['id']);
        $incMoney = $userInc['user_money'] + $money - ($money*0.05);
        $userInc['user_money'] = $incMoney;
        $userDec['user_money'] = $userDec['user_money'] - $money;
        redisd() -> HSet("user", $connection->uid, $userDec);
        redisd() -> HSet("user", $user['id'], $userInc);
        //添加转账记录
        $data = array(
            "formid"=>$connection->uid,
            "toid"=>$user['id'],
            "money"=>$money,
            "add_time"=>time()
        );
        db("zhuang") -> insert($data);
        sszhuang($connection->worker, $user['id'],$incMoney);
        return array("ok"=>1,"ws"=>"26","money"=>($myuser['user_money']-$money));
    }else{
        return array("ok"=>2,"content"=>"无此账户");
    }
}

/**
 * 实时更新转账信息
 */
function sszhuang($worker,$id,$money){
    $connections = $worker->connections;
    foreach ($connections as $connection){
        if(isset($connection->uid) && $connection->uid == $id){
            $data = array(
                "ws" => 120,
                "ok" => 1,
                "money" => $money
            );
            send($data, $connection);
        }
    }
}



/**
 * 找回密码
 * @param unknown $message
 * @param unknown $connection
 */
function passBack($message,$connection){
   $userModel = db("users");
   $user = $userModel->where("mobile={$message['mobile']}")->find();//查找用户是否有
   if($user){
       if($user['wenti']==$message['wenti']){//判断问题密码是否正确
           $aa = $userModel->where("mobile={$message['mobile']}")->update(array("pwd"=>$message['pwd']));
          
               return array("ok"=>1,"ws"=>"17");
         
       }else{
           return array("ok"=>2,"content"=>"答案验证错误");
       }
   }else{
       return array("ok"=>2,"content"=>"无此账户");
   }
}

/**
 * 充值记录
 * @param unknown $message
 * @param unknown $connection
 * @return number[]|string[]|\think\Collection[]|\think\db\false[]|PDOStatement[]
 */
function rechargeLog($message,$connection){
    $rechargeModel = db("recharge");
    $list = $rechargeModel->field("add_time,money")->where("user_id={$connection->uid} and status=1")->order("id desc")->limit(0,10)->select();
    foreach($list as $key=> $val){//循环遍历时间转换
        $list[$key]['add_time']=date('m-d H:i:s',$val['add_time']);
    }
    return array(
        "ws"=>27,
        "ok"=>"1",
        "list"=>$list
    );
}



/**
 * 提现记录
 * @param unknown $message
 * @param unknown $connection
 */
function withLog($message,$connection){
    $widthraswalsModel = db("widthraswals");
     $list = $widthraswalsModel->field("add_time,money")->where("user_id={$connection->uid} and status=1")->order("id desc")->limit(0,10)->select();
     foreach($list as $key=> $val){//循环遍历时间转换
         $list[$key]['add_time']=date('m-d H:i:s',$val['add_time']);
     }
     return array(
         "ws"=>28,
         "ok"=>"1",
         "list"=>$list
     );
}

/**
 * 转账记录
 * @param unknown $message
 * @param unknown $connection
 * @return number[]|string[]|\think\Collection[]|\think\db\false[]|PDOStatement[]
 */
function zhuanLog($message,$connection){
    $widthraswalsModel = db("zhuang");
    $lists['z'] = $widthraswalsModel->field("add_time,money,toid")->where("formid={$connection->uid}")->order("id desc")->limit(0,10)->select();
    $lists['c'] = $widthraswalsModel -> field("formid,add_time,money") -> where("toid={$connection->uid}") -> order("id desc") -> limit(0,10) -> select();
//     dump($lists);
    $list = array();
    foreach($lists as $k => $val){//循环遍历时间转换
        foreach ($val as $v){
            if ($k == 'c'){
                $v['type'] = 2;
            }elseif ($k == "z"){
                $v['type'] = 1;
            }
            $v['add_time']=date('m-d H:i:s',$v['add_time']);
            $list[] = $v;
        }
    }
//     dump($list);
    return array(
        "ws"=>29,
        "ok"=>"1",
        "list"=>$list
    );
}

/**
 * 下庄
 * @param unknown $message
 * @param unknown $connection
 * @return number[]|string[]
 */
function xiazhuang($message,$connection){
    $json = json_decode($message['data'],true);
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"uid null");
    }
    if(!isset($connection->homeid)){
        return array("ok"=>2,"content"=>"31 fail");
    }
    $home = redisd() -> HGet("homes", $connection->homeid);
    $home['role_user_id'] = 0;
    $home['rolemoney'] = 0;
    redisd() -> HSet("homes", $connection->homeid, $home);
    $array = array("ws"=>86,"ok"=>1,"money"=>0);
  
    //发送给在房间的每个人，下庄了
    sendByHomeIdAndWorker($connection->worker,$connection->homeid,$array);
    
    return array("ok"=>1,"ws"=>"35");
}

/**
 * 判断客户是否在线
 * @param unknown $worker
 * @param unknown $id
 */
function is_login($worker,$id){
    $bool = false;
    $connections = $worker->connections;
    foreach ($connections as $connection){
        
        if(isset($connection->uid)&&$connection->uid==$id){
            $bool= true;
            break;
        }
    } 
    return $bool;
}


/**
 * 通过id发送数据
 * @param unknown $worker
 * @param unknown $id
 * @param unknown $array
 */
function sendById($worker,$id,$array){
    $connections = $worker->connections;
    foreach ($connections as $connection){
        if(isset($connection->uid)&&$connection->uid==$id){
           send($array, $connection);
        }
    }
    
}


/**
 * 根据房间号发送信息
 * @param unknown $worker
 * @param unknown $id
 * @param unknown $array
 */
function sendByHomeId($connection,$id,$array){
    $worker = $connection->worker;
    $connections = $worker->connections;
    foreach ($connections as $connection){
        if(isset($connection->homeid)&&$connection->homeid==$id){
            send($array, $connection);
        }
    }
}

/**
 * 根据房间号发送信息
 * @param unknown $worker
 * @param unknown $id
 * @param unknown $array
 */
function sendByHomeIdAndWorker($worker,$id,$array){
    $connections = $worker->connections;
    foreach ($connections as $connection){
        if(isset($connection->homeid)&&$connection->homeid==$id){
           send($array, $connection);
        }
    }
}
/**
 * 心跳链接
 * @param unknown $message
 * @param unknown $connection
 */
function xintiao($message,$connection){
    $connection->lastMessageTime = time();
    return array("ok"=>1,"ws"=>"0");
}


/**
 * 房间成员
 * @param unknown $message
 * @param unknown $connection
 */
function getRen($message,$connection){
    if(!isset($connection->uid)){
        return array("ok"=>2,"content"=>"uid null");
    }
    if(!isset($connection->homeid)){
        return array("ok"=>2,"content"=>"31 fail");
    }
    $homeid = $connection->homeid;
    $userIdList = redisd() -> HGet("homeUser",$homeid);
    if($userIdList){
        foreach ($userIdList as $row){
            $users[] = redisd() -> HGet("user", $row);
        }
        if ($homeid == 1){
            $jqs = db("users") -> where("user_type=1") -> select();
            foreach ($jqs as $v){
                $users[] = $v;
            }
        }
    }else{
        $users = 0;
    }
    
    return array(
        "ws"=>36,
        "ok"=>"1",
        "list"=>$users
    );   
}

//检测版本
function ckeckBb($connection){
    $bb = db("config")->find(12);
    return array(
        "ws"=>1,
        "bbh"=>$bb['values']
    );
}








