<?php
namespace app\index\controller;
use app\index\controller\Commonse;
use think\Db;
use think\Request;
class Data extends Commonse
{   
    
    private $user;

    public function __construct()
    { // 自动加载
        parent::__construct();
    }
    
    
    /**
     * 发喇叭
     * @param unknown $client_id
     * @param unknown $data
     */
    public function notice(){
        if (Request::instance()->isPost()) {
            $state = input("state");
            $ids = input("text/a");
            if ($state == 1){
                if (!$ids){
                    $this->error("选中内容有误！！！");
                }else {
                    $text = db("notice")->field("text")->where('id','in',$ids)->select();
                    foreach ($text as $k => $v){
                        $returns[] = $v['text'];
                    }
                    $return83 = array(
                        "text"=>$returns
                    );
                    sendToAll(2083, $return83);
                    db("notice")->where('id=1')->update(['text'=>$state]);
                    db("notice")->where('id','in',$ids)->update(['state'=>$state]);
                    $this->success("操作成功！！！");
                }
            }else {
                $return83 = array(
                    "text"=>0
                );
                sendToAll(2083, $return83);
                db("notice")->where('id=1')->update(['text'=>$state]);
                db("notice")->where("id!=1")->update(['state'=>$state]);
                $this->success("操作成功！！！");
            }
        }
        $noticeList = db("notice")->where("id!=1")->select();
        $noticeType = db("notice")->find(1);
        $this->assign("listType",(int)$noticeType['text']);
        $this->assign("list",$noticeList);
        return $this->fetch();
    }
    
    /**
     * 添加喊话内容
     * @return unknown
     */
    public function addNotice(){
        if (Request::instance()->isPost()) {
            $text = input("text","","htmlspecialchars,trim");
            if (!$text){
                $this->error("输入内容有误！！！");
            }else {
                $return83 = array(
                    "text"=>$text
                );
                db("notice")->insert($return83);
                $this->success("添加成功！！！");
            }
        }
        return $this->fetch();
    }
    
    /**
     * 删除喊话内容
     */
    public function delNotice(){
        $id = input("id","","intval");
        if (!$id){
            $this->error("参数错误！！！");
        }else {
            db("notice")->where('id='.$id)->delete();
            $this->success("删除成功！！！");
        }
    }
    
    ///////////////////
    function cz1history(){//充值记录
        $times = input('times', ''); // 开始时间
        $timed = input('timed', ''); // 结束时间
        $se=input('se','');
        if ($times > $timed) {
            $this->error('参数错误');
        }
        $where = '';
        if (! empty($times) || ! empty($timed)) { // 查询
            // 转换为时间戳
            $times = strtotime($times); // 开始时间
            $timed = strtotime($timed) + 24 * 60 * 60;
            $where = " add_time>=" . $times . " and add_time<=" . $timed; // 结束时间为当天23点
        }
        switch ($se) {
            case 6:
                { // 今天
                    $times = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                    $timed = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 1:
                { // 昨天
                    $times = mktime(0, 0, 0, date('m'), date('d') - 1, date('Y'));
        
                    $timed = mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 1;
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 2:
                { // 本周
                    $times =mktime(0, 0 , 0,date("m"),date("d")-date("w")+1,date("Y"));
                    $timed=mktime(23,59,59,date("m"),date("d")-date("w")+7,date("Y"));
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 3:
                { // 上周
                    $times = mktime(0, 0, 0, date('m'), date('d') - date('w') + 1 - 7, date('Y'));
        
                    $timed = mktime(23, 59, 59, date('m'), date('d') - date('w') + 7 - 7, date('Y'));
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 4:
                { // 本月
                     
                    $times = mktime(0, 0, 0, date('m'), 1, date('Y'));
                    $timed = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 5:
                { // 上月
                    $times =  mktime(0, 0 , 0,date("m")-1,1,date("Y"));
                    $timed = mktime(23,59,59,date("m") ,0,date("Y"));
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
        }
        $czMode=Db::name('Recharge');
        $list=$czMode->alias('r')->field("r.*,u.nickname as uname")->join("users u","u.id=r.user_id")->where($where)->order("id desc")->paginate(50);
        $this->assign("list", $list);
        
        //充值总金额
        $czMoney=$czMode->alias('r')->join("users u","u.id=r.user_id")->where($where)->sum('money');
        if (!$czMoney) $czMoney = 0;
        $this->assign("czMoney", $czMoney);
        $page = $list->render();
       return $this->fetch();
    }
    function jian(){//充值记录
        $times = input('times', ''); // 开始时间
        $timed = input('timed', ''); // 结束时间
        $se=input('se','');
        
        $where = "";
        if ($times > $timed && !empty($timed)) {
            $this->error('参数错误');
        }
        if (!empty($times) && !empty($timed)) { // 查询
            // 转换为时间戳
            $times = strtotime($times); // 开始时间
            $timed = strtotime($timed) + 24 * 60 * 60;
            $where = " add_time>=" . $times . " and add_time<=" . $timed; // 结束时间为当天23点
        }elseif (!empty($times) && empty($timed)) {
            $times = strtotime($times); // 开始时间
            $where = " add_time>=" . $times; // 结束时间为当天23点
        }elseif (empty($times) && !empty($timed)) {
            $timed = strtotime($timed) + 24 * 60 * 60;
            $where = " add_time<=" . $timed; // 结束时间为当天23点
        }
        
        switch ($se) {
            case 6:
                { // 今天
                    $times = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                    $timed = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 1:
                { // 昨天
                    $times = mktime(0, 0, 0, date('m'), date('d') - 1, date('Y'));
                    
                    $timed = mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 1;
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 2:
                { // 本周
                    $times =mktime(0, 0 , 0,date("m"),date("d")-date("w")+1,date("Y"));
                    $timed=mktime(23,59,59,date("m"),date("d")-date("w")+7,date("Y"));
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 3:
                { // 上周
                    $times = mktime(0, 0, 0, date('m'), date('d') - date('w') + 1 - 7, date('Y'));
                    
                    $timed = mktime(23, 59, 59, date('m'), date('d') - date('w') + 7 - 7, date('Y'));
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 4:
                { // 本月
                    
                    $times = mktime(0, 0, 0, date('m'), 1, date('Y'));
                    $timed = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 5:
                { // 上月
                    $times =  mktime(0, 0 , 0,date("m")-1,1,date("Y"));
                    $timed = mktime(23,59,59,date("m") ,0,date("Y"));
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
        }
        $czMode=Db::name('czh');
        $list=$czMode->alias('c')->field("c.*,u.nickname as uname")->join("users u","u.id=c.yid")->where($where)->order("id desc")->paginate(50);
        $this->assign("list", $list);
        
        //充值总金额
        $czMoney=$czMode->alias('c')->join("users u","u.id=c.yid")->where($where)->sum('money');
        if (!$czMoney) $czMoney = 0;
        $this->assign("czMoney", $czMoney);
        $page = $list->render();
        return $this->fetch();
    }
    function dui(){
        $id=input('id');
        $user_id=input('user_id',0,"intval");
        $money=input('money');
        $userMode=Db::name('users');
        $reMode=Db::name('recharge');
        if($reMode->where('user_id='.$user_id." and id=".$id)->update(array('status'=>1))){
            if($userMode->where('id='.$user_id)->setInc('user_money',$money)){
                $this->success('已充值');
            }else{
                $this->error('充值失败');
            }
        }else{
            $this->error('状态修改有误');
        }
        
        
    }
    function eror(){
        $id=input('id',0,'intval');
        $reMode=Db::name('recharge');
        if($reMode->where("id=".$id)->update(array('status'=>2))){
            $this->success('已拒绝');
        }else{
            $this->error('状态修改失败');
        }
    }
    
    function tixian(){//提现记录
        $keyword = input('get.keyword', '', 'htmlspecialchars,trim');
        $where = array();
        if (!empty($keyword)) {//查询
            $where['user_id'] = array('like', "%$keyword%");
        }
        $mode=Db::name('Widthraswals');
        $list=$mode->where($where)->order('id desc')->paginate(50);
        $this->assign("list", $list);
        $page=$list->render();
        return  $this->fetch();
    }
    public function zzhistory(){//用户之间的转账记录
        $keyword = input('get.keyword','');
        if($keyword){
            $where = "formid={$keyword}";
        }else{
            $where = "";
        }
        $list=db('Zhuang')->where($where)->order('id desc')->paginate(50);
        $this->assign("list", $list);
        return $this->fetch();
    }
    function dui1(){
        $id=input('id');
        $user_id=input('user_id',0,"intval");
        $money=input('money');
        $tx_type=input('type');
        $userMode=Db::name('users');
        $reMode=Db::name('widthraswals');
        $usersss=$userMode->where('id='.$user_id)->select(); 
        if($tx_type==1){//余额提现
            if($reMode->where('user_id='.$user_id." and id=".$id)->update(array('status'=>1))){
                if($userMode->where('id='.$user_id)->setDec('user_money',$money)){//余额
                    $this->success('已充值');
                }else{
                    $this->error('充值失败');
                }
            }else{
                $this->error('状态修改有误');
            }
            
        }else{//佣金提现
            if($reMode->where('user_id='.$user_id." and id=".$id)->update(array('status'=>1))){
                if($userMode->where('id='.$user_id)->setDec('com_money',$money)){//余额
                    $this->success('已充值');
                }else{
                    $this->error('充值失败');
                }
            }else{
                $this->error('状态修改有误');
            }
            
            
        }
        
    
    
    }
    function eror1(){
        $id=input('id',0,'intval');
        $reMode=Db::name('widthraswals');
        if($reMode->where("id=".$id)->update(array('status'=>2))){
            $this->success('已拒绝');
        }else{
            $this->error('状态修改失败');
        }
    }
    
 
    function order()
    {
        $times = input('get.times', ''); // 开始时间
        $timed = input('get.timed', ''); // 结束时间
        $se = input('get.se', ''); //
        if ($times > $timed) {
            $this->error('参数错误');
        }
        $where = array();
        if (! empty($times) || ! empty($timed)) { // 查询
                                                  // 转换为时间戳
            $times = strtotime($times); // 开始时间
            $timed = strtotime($timed) + 24 * 60 * 60;
            $where = " add_time>=" . $times . " and add_time<=" . $timed; // 结束时间为当天23点
        }
        
        switch ($se) {
            case 6:
                { // 今天
                    $times = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                    $timed = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 1:
                { // 昨天
                    $times = mktime(0, 0, 0, date('m'), date('d') - 1, date('Y'));
                    
                    $timed = mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 1;
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 2:
                { // 本周
                    $times =mktime(0, 0 , 0,date("m"),date("d")-date("w")+1,date("Y"));
                   $timed=mktime(23,59,59,date("m"),date("d")-date("w")+7,date("Y"));
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 3:
                { // 上周
                    $times = mktime(0, 0, 0, date('m'), date('d') - date('w') + 1 - 7, date('Y'));
                    
                    $timed = mktime(23, 59, 59, date('m'), date('d') - date('w') + 7 - 7, date('Y'));
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 4:
                { // 本月
                   
                    $times = mktime(0, 0, 0, date('m'), 1, date('Y'));
                    $timed = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
            case 5:
                { // 上月
                    $times =  mktime(0, 0 , 0,date("m")-1,1,date("Y"));
                    $timed = mktime(23,59,59,date("m") ,0,date("Y"));
                    $where = " add_time>=" . $times . " and add_time<=" . $timed;
                }
                ;
                break;
        }
        $config=db('config');
        $kou=$config->where('id=3')->find();
        $mode = db('order');
        $zmoney = 0.00;
        $list = $mode->alias('a')->
        field('a.num,a.add_time,a.user_id,a.home_id,b.nickname,b.url,sum(a.money) as moneyz')
        ->join('app_users b','b.id=a.user_id')->where('order_type=0')->where($where)
            ->order('add_time desc')->group('num,user_id')
            ->paginate(50);
        foreach($list as $k=>$value){
            $kkd=$mode->alias('a')->field('sum(a.money) as moneyza')
            ->join('app_users b','b.id=a.user_id')
            ->where('order_type=0 and is_winning=1 and num='.$value['num'])->where($where)
            ->order('add_time desc')->group('num,user_id')->select();
            if(!empty($kkd)){
                $iid['jjj']=$kkd[0]['moneyza']*2-$value['moneyz'];
                $list[$k]+=$iid;
            }else{
                $iid['jjj']=0-$value['moneyz'];
                $list[$k]+=$iid;
            }
            $zmoney += $value['moneyz'];
        }
        $this->assign("zmoney",$zmoney);
        $this->assign("list", $list);
        $page = $list->render();
       return $this->fetch();
    } 
 
    function time(){//实时统计
       return $this->fetch('tongji');   
    }
    function dian(){//实时统计房间人
    /*   $home_id=db('Home');
       $home_user=db('HomeUser');*/
       
       return $this->fetch();   
    }

public function homeuser(){
    return $this->fetch();  
}

public  function daili(){
    $times = input('get.times', ''); // 开始时间
    $timed = input('get.timed', ''); // 结束时间
    $se = input('get.se', ''); //
    if ($times > $timed) {
        $this->error('参数错误');
    }
    $where = array();
    if (! empty($times) || ! empty($timed)) { // 查询
        // 转换为时间戳
        $times = strtotime($times); // 开始时间
        $timed = strtotime($timed) + 24 * 60 * 60;
        $where = " add_time>=" . $times . " and add_time<=" . $timed; // 结束时间为当天23点
    }
    
    switch ($se) {
        case 6:
            { // 今天
                $times = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                $timed = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                $where = " add_time>=" . $times . " and add_time<=" . $timed;
            }
            ;
            break;
        case 1:
            { // 昨天
                $times = mktime(0, 0, 0, date('m'), date('d') - 1, date('Y'));
                
                $timed = mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 1;
                $where = " add_time>=" . $times . " and add_time<=" . $timed;
            }
            ;
            break;
        case 2:
            { // 本周
                $times =mktime(0, 0 , 0,date("m"),date("d")-date("w")+1,date("Y"));
                $timed=mktime(23,59,59,date("m"),date("d")-date("w")+7,date("Y"));
                $where = " add_time>=" . $times . " and add_time<=" . $timed;
            }
            ;
            break;
        case 3:
            { // 上周
                $times = mktime(0, 0, 0, date('m'), date('d') - date('w') + 1 - 7, date('Y'));
                
                $timed = mktime(23, 59, 59, date('m'), date('d') - date('w') + 7 - 7, date('Y'));
                $where = " add_time>=" . $times . " and add_time<=" . $timed;
            }
            ;
            break;
        case 4:
            { // 本月
                
                $times = mktime(0, 0, 0, date('m'), 1, date('Y'));
                $timed = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
                $where = " add_time>=" . $times . " and add_time<=" . $timed;
            }
            ;
            break;
        case 5:
            { // 上月
                $times =  mktime(0, 0 , 0,date("m")-1,1,date("Y"));
                $timed = mktime(23,59,59,date("m") ,0,date("Y"));
                $where = " add_time>=" . $times . " and add_time<=" . $timed;
            }
            ;
            break;
    }
    $rebate = db('rebate');
    $mode = $rebate -> where($where) ->order('add_time desc') -> select();
    $dlList = [];
    $dlMoney = 0;
    $ptMoney = 0;
    foreach ($mode as $res){
        if ($res['status'] == 0){
            $dlList[] = $res;
            $dlMoney += $res['money'];
        }elseif ($res['status'] == 1){
            $ptMoney += $res['money'];
        }
    }
    $this -> assign('dailis',$dlList);
    $this -> assign("dlMoney",$dlMoney);
    $this -> assign("ptMoney",$ptMoney);
    return $this->fetch();
}
    


public function xiang(){//用户下注详情
    $times = input('times', ''); // 开始时间
    $timed = input('timed', ''); // 结束时间
    $se=input('se','');
    if ($times > $timed) {
        $this->error('参数错误');
    }
    $where = '';
    if (! empty($times) || ! empty($timed)) { // 查询
        // 转换为时间戳
        $times = strtotime($times); // 开始时间
        $timed = strtotime($timed) + 24 * 60 * 60;
        $where = " add_time>=" . $times . " and add_time<=" . $timed; // 结束时间为当天23点
    }
    switch ($se) {
        case 6:
            { // 今天
                $times = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                $timed = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                $where = " add_time>=" . $times . " and add_time<=" . $timed;
            }
            ;
            break;
        case 1:
            { // 昨天
                $times = mktime(0, 0, 0, date('m'), date('d') - 1, date('Y'));
                
                $timed = mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 1;
                $where = " add_time>=" . $times . " and add_time<=" . $timed;
            }
            ;
            break;
        case 2:
            { // 本周
                $times =mktime(0, 0 , 0,date("m"),date("d")-date("w")+1,date("Y"));
                $timed=mktime(23,59,59,date("m"),date("d")-date("w")+7,date("Y"));
                $where = " add_time>=" . $times . " and add_time<=" . $timed;
            }
            ;
            break;
        case 3:
            { // 上周
                $times = mktime(0, 0, 0, date('m'), date('d') - date('w') + 1 - 7, date('Y'));
                
                $timed = mktime(23, 59, 59, date('m'), date('d') - date('w') + 7 - 7, date('Y'));
                $where = " add_time>=" . $times . " and add_time<=" . $timed;
            }
            ;
            break;
        case 4:
            { // 本月
                
                $times = mktime(0, 0, 0, date('m'), 1, date('Y'));
                $timed = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
                $where = " add_time>=" . $times . " and add_time<=" . $timed;
            }
            ;
            break;
        case 5:
            { // 上月
                $times =  mktime(0, 0 , 0,date("m")-1,1,date("Y"));
                $timed = mktime(23,59,59,date("m") ,0,date("Y"));
                $where = " add_time>=" . $times . " and add_time<=" . $timed;
            }
            ;
            break;
    }
    $id=input('id');
    if ($id){        
        $Model =db('order');
        $list=$Model->where('user_id='.$id) ->where($where) ->order('add_time desc')->select();
        $zmoney = $Model->field("sum(money)") -> where('user_id='.$id) -> where($where) -> find();
        $this->assign("uid",$id);
        $this->assign("zmoney",$zmoney['sum(money)']);
        $this->assign("list", $list);
    }
    return $this->fetch();
} 
    
}